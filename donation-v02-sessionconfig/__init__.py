from otree.api import *

import random

doc = """
A donation game
"""


class C(BaseConstants):
    NAME_IN_URL = "donation"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    for player in subsession.get_players():
        player.sees_flood = random.choice([True, False])


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    sees_flood = models.BooleanField()
    donation = models.CurrencyField(min=cu(0))


def donation_max(player):
    return cu(player.session.config["endowment"])


# PAGES
class MyPage(Page):
    @staticmethod
    def is_displayed(player):
        return player.sees_flood


class DonationPage(Page):
    form_model = "player"
    form_fields = ["donation"]


class Results(Page):
    pass


page_sequence = [MyPage, DonationPage, Results]
