from otree.api import *

import random

doc = """
A donation game
"""


class C(BaseConstants):
    NAME_IN_URL = "donation"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    for player in subsession.get_players():
        player.sees_flood = random.choice([True, False])


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    sees_flood = models.BooleanField()
    donation = models.CurrencyField(min=cu(0))
    education = models.IntegerField(
        label="How many years of formal education have you completed? Please include all years of primary school, secondary school, and any further or higher education.",
        choices=[
            [1, "Less than 11 years (Left school before completing GCSEs)"],
            [2, "11-12 years (Completed GCSEs or equivalent)"],
            [3, "13-14 years (Completed A-Levels or equivalent)"],
            [4, "15-16 years (Completed Bachelor's degree or equivalent)"],
            [5, "17-18 years (Completed Master's degree or equivalent)"],
            [6, "19+ years (Completed Doctorate or equivalent)"],
        ],
    )


def donation_max(player):
    return cu(player.session.config["endowment"])


# PAGES
class MyPage(Page):
    @staticmethod
    def is_displayed(player):
        return player.sees_flood


class DonationPage(Page):
    form_model = "player"
    form_fields = ["donation", "education"]


class Results(Page):
    pass


page_sequence = [MyPage, DonationPage, Results]
