from otree.api import *

import random
import time

doc = """
A donation game
"""


class C(BaseConstants):
    NAME_IN_URL = "donation"
    PLAYERS_PER_GROUP = 2
    NUM_ROUNDS = 1

    CA_ROLE = "CA"
    CHOOSER_ROLE = "CHOOSER"
    DELIBERATE_FOR = 20


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    for group in subsession.get_groups():
        group.sees_flood = random.choice([True, False])


class Group(BaseGroup):
    sees_flood = models.BooleanField()
    min_donation = models.CurrencyField(label="Minimum donation", min=cu(0))


class Player(BasePlayer):
    start_time = models.FloatField()
    may_continue = models.BooleanField()

    donation = models.CurrencyField()
    education = models.IntegerField(
        label="How many years of formal education have you completed? Please include all years of primary school, secondary school, and any further or higher education.",
        choices=[
            [1, "Less than 11 years (Left school before completing GCSEs)"],
            [2, "11-12 years (Completed GCSEs or equivalent)"],
            [3, "13-14 years (Completed A-Levels or equivalent)"],
            [4, "15-16 years (Completed Bachelor's degree or equivalent)"],
            [5, "17-18 years (Completed Master's degree or equivalent)"],
            [6, "19+ years (Completed Doctorate or equivalent)"],
        ],
    )


def donation_min(player):
    if player.role == C.CA_ROLE:
        return cu(0)
    else:
        return player.group.min_donation


def donation_max(player):
    return cu(player.session.config["endowment"])


def min_donation_max(player):
    return cu(player.session.config["endowment"])


# PAGES
class MyPage(Page):
    form_model = "player"
    form_fields = ["may_continue"]

    @staticmethod
    def is_displayed(player):
        if player.field_maybe_none("start_time") is None:
            player.start_time = time.perf_counter()

        return player.group.sees_flood

    @staticmethod
    def js_vars(player):
        current_time = time.perf_counter()
        spent = current_time - player.start_time

        return dict(remaining_time=(C.DELIBERATE_FOR - spent) * 1000)


def may_continue_error_message(player, value):
    current_time = time.perf_counter()

    if current_time - player.start_time < C.DELIBERATE_FOR:
        return "Please deliberate. 🙏"


class ChoiceArchitectDecision(Page):
    form_model = "group"
    form_fields = ["min_donation"]

    @staticmethod
    def is_displayed(player):
        return player.role == C.CA_ROLE


class DonationPage(Page):
    form_model = "player"
    form_fields = ["donation", "education"]

    timeout_seconds = 60

    @staticmethod
    def before_next_page(player, timeout_happened):
        if timeout_happened:
            print("OMG! The timeout happened, flippin' heck.")


class MyWaitPage(WaitPage):
    pass


class Results(Page):
    pass


page_sequence = [MyPage, ChoiceArchitectDecision, MyWaitPage, DonationPage, Results]
