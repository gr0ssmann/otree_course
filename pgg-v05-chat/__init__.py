from otree.api import *

import random


doc = """
A public-goods game with varying efficiencies and a history table
"""


class C(BaseConstants):
    NAME_IN_URL = "pgg"
    PLAYERS_PER_GROUP = 2
    NUM_ROUNDS = 4
    EFFICIENCIES = [2, 5]
    ENDOWMENT = cu(10)


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    if subsession.round_number == 1:
        # assign parameters in round 1

        for group in subsession.get_groups():
            group.info = random.choice([True, False])  # assigns treatment to each group

            for player, e in zip(group.get_players(), C.EFFICIENCIES):
                player.efficiency = e
    else:
        # in further rounds, use round 1 values

        for group in subsession.get_groups():
            group.info = group.in_round(1).info

            for player in group.get_players():
                player.efficiency = player.in_round(1).efficiency


class Group(BaseGroup):
    info = models.BooleanField()  # treatment variable


class Player(BasePlayer):
    efficiency = models.FloatField()
    contribution = models.CurrencyField(
        label="Your contribution to the fund", max=C.ENDOWMENT
    )


# PAGES
def set_dropout(player, timeout_happened):
    if timeout_happened:
        player.participant.vars["dropout"] = True
        player.participant.vars["dropout_guilty"] = True

        for other in player.group.get_players():
            if other.id_in_group != player.id_in_group:
                other.participant.vars["dropout"] = True
                other.participant.vars["dropout_guilty"] = False


def handle_dropout(player, upcoming_apps):
    if "dropout" in player.participant.vars and player.participant.vars["dropout"]:
        return "dropout_handler"


class Instructions(Page):
    # timeout_seconds = 10
    before_next_page = set_dropout
    app_after_this_page = handle_dropout

    @staticmethod
    def is_displayed(player):
        return player.round_number == 1


class Decision(Page):
    # timeout_seconds = 10
    before_next_page = set_dropout
    app_after_this_page = handle_dropout

    form_model = "player"
    form_fields = ["contribution"]

    @staticmethod
    def vars_for_template(player):
        htable = []

        for hgroup in player.group.in_previous_rounds():
            if player.id_in_group == 1:
                myself = hgroup.get_player_by_id(1).contribution
                partner = hgroup.get_player_by_id(2).contribution
            else:
                myself = hgroup.get_player_by_id(2).contribution
                partner = hgroup.get_player_by_id(1).contribution

            htable.append(
                [
                    hgroup.round_number,
                    myself,
                    partner,
                ]
            )

        return dict(htable=htable)


class ResultsWaitPage(WaitPage):
    app_after_this_page = handle_dropout

    def after_all_players_arrive(group):
        fund = 0

        for player in group.get_players():
            if (
                "dropout" in player.participant.vars
                and player.participant.vars["dropout"]
            ):
                return

            fund += player.efficiency * player.contribution

        share = fund / C.PLAYERS_PER_GROUP

        for player in group.get_players():
            player.payoff = cu(C.ENDOWMENT - player.contribution + share)


class IntermediateResult(Page):
    # timeout_seconds = 10
    before_next_page = set_dropout
    app_after_this_page = handle_dropout


class Results(Page):
    # timeout_seconds = 10
    before_next_page = set_dropout
    app_after_this_page = handle_dropout

    @staticmethod
    def is_displayed(player):
        return player.round_number == C.NUM_ROUNDS


page_sequence = [Instructions, Decision, ResultsWaitPage, IntermediateResult, Results]
