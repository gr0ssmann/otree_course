from otree.api import *

import random


doc = """
Apartment study
"""


class C(BaseConstants):
    NAME_IN_URL = "conjoint"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1

    ATTRIBUTES = {
        "rent": [400, 500, 600, 800, 900, 1000, 1500],
        "size": [15, 20, 30, 50, 120],
        "neigh": ["Sülz", "Hürth", "Kalk", "Zollstock", "Lindenthal"],
    }
    HOWMANY = 10


def creating_session(subsession):
    for player in subsession.get_players():
        for i in range(C.HOWMANY):
            Stimuli.create(
                player=player,
                i=i,
                rent1=random.choice(C.ATTRIBUTES["rent"]),
                rent2=random.choice(C.ATTRIBUTES["rent"]),
                size1=random.choice(C.ATTRIBUTES["size"]),
                size2=random.choice(C.ATTRIBUTES["size"]),
                neigh1=random.choice(C.ATTRIBUTES["neigh"]),
                neigh2=random.choice(C.ATTRIBUTES["neigh"]),
            )


def custom_export(players):
    yield [
        "player",
        "session",
        "i",
        "rent1",
        "rent2",
        "size1",
        "size2",
        "neigh1",
        "neigh2",
        "choice",
    ]

    for row in Stimuli.filter():
        yield [
            row.player.participant.code,
            row.player.session.code,
            row.i,
            row.rent1,
            row.rent2,
            row.size1,
            row.size2,
            row.neigh1,
            row.neigh2,
            row.choice,
        ]


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    counter = models.IntegerField(initial=0)
    may_continue = models.BooleanField()


def may_continue_error_message(player, value):
    if player.counter < C.HOWMANY:
        return "You cannot continue yet."


class Stimuli(ExtraModel):
    player = models.Link(Player)
    i = models.IntegerField()
    rent1 = models.IntegerField()
    rent2 = models.IntegerField()
    size1 = models.IntegerField()
    size2 = models.IntegerField()
    neigh1 = models.StringField()
    neigh2 = models.StringField()
    choice = models.IntegerField()


# PAGES
class MyPage(Page):
    form_model = "player"
    form_fields = ["may_continue"]

    @staticmethod
    def live_method(player, data):
        if isinstance(data, list):
            if data[0] == "get":
                current_stimulus = Stimuli.filter(player=player, i=player.counter)[0]

                return {
                    0: [
                        "conjoint",
                        current_stimulus.rent1,
                        current_stimulus.size1,
                        current_stimulus.neigh1,
                        current_stimulus.rent2,
                        current_stimulus.size2,
                        current_stimulus.neigh2,
                    ]
                }
            elif data[0] == "submit" and data[1] in [1, 2]:
                current_stimulus = Stimuli.filter(player=player, i=player.counter)[0]

                Stimuli.create(
                    player=player,
                    i=player.counter,
                    rent1=current_stimulus.rent1,
                    size1=current_stimulus.size1,
                    neigh1=current_stimulus.neigh1,
                    rent2=current_stimulus.rent2,
                    size2=current_stimulus.size2,
                    neigh2=current_stimulus.neigh2,
                    choice=data[1],
                )

                player.counter += 1

                if player.counter < C.HOWMANY:
                    return {0: ["proceed"]}
                else:
                    return {0: ["finish"]}


page_sequence = [MyPage]
