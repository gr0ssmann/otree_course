from otree.api import *


doc = """
Diary game (somebody's watching me)
"""


class C(BaseConstants):
    NAME_IN_URL = "diary"
    PLAYERS_PER_GROUP = 2
    NUM_ROUNDS = 1

    SENDER_ROLE = "SENDER"  # id 1
    RECEIVER_ROLE = "RECEIVER"  # id 2


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    entry = models.LongStringField(initial="")


class Player(BasePlayer):
    pass


# PAGES
class MyPage(Page):
    @staticmethod
    def live_method(player, data):
        match data:
            case ["get"]:
                return {2: player.group.entry}
            case ["set", str]:
                player.group.entry = data[1]

                return {2: data[1]}


page_sequence = [MyPage]
