from otree.api import *

import random

doc = """
A donation game
"""


class C(BaseConstants):
    NAME_IN_URL = "donation"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1
    ENDOWMENT = cu(10)


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    for player in subsession.get_players():
        player.sees_flood = random.choice([True, False])


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    sees_flood = models.BooleanField()
    donation = models.CurrencyField(min=cu(0), max=C.ENDOWMENT)


# PAGES
def set_dropout(player, timeout_happened):
    if timeout_happened:
        player.participant.vars["dropout"] = True


def handle_dropout(player, upcoming_apps):
    if "dropout" in player.participant.vars and player.participant.vars["dropout"]:
        return "dropout_handler"


class MyPage(Page):
    timeout_seconds = 10
    before_next_page = set_dropout
    app_after_this_page = handle_dropout

    @staticmethod
    def is_displayed(player):
        return player.sees_flood


class DonationPage(Page):
    timeout_seconds = 10
    before_next_page = set_dropout
    app_after_this_page = handle_dropout
    form_model = "player"
    form_fields = ["donation"]


class Results(Page):
    pass


page_sequence = [MyPage, DonationPage, Results]
