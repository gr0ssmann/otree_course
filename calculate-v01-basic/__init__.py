from otree.api import *


doc = """
Calculation game with live pages
"""


class C(BaseConstants):
    NAME_IN_URL = "calculate"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1
    TASKS = [(4, 9), (7, 2), (1, 1), (4, 4), (2, 6)]
    # index:  0       1       2       3       4


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    current_task = models.IntegerField(initial=0)  # state
    may_continue = models.BooleanField()  # sentinel
    score = models.IntegerField(initial=0)


def may_continue_error_message(player, value):
    # a sentinel field disregards the "value" argument

    if player.current_task <= 4:
        return "You cannot continue yet."


# PAGES
class MyPage(Page):
    form_model = "player"
    form_fields = ["may_continue"]

    @staticmethod
    def live_method(player, data):
        if isinstance(data, list):
            if data[0] == "get":
                return {0: ["task", C.TASKS[player.current_task]]}
            elif data[0] == "submit" and isinstance(data[1], int):
                correct_solution = sum(C.TASKS[player.current_task])
                player_solution = data[1]

                if player_solution == correct_solution:
                    player.score += 1

                player.current_task += 1

                if player.current_task > 4:
                    return {0: ["finish"]}
                else:
                    return {0: ["proceed"]}


class Results(Page):
    pass


page_sequence = [MyPage, Results]
