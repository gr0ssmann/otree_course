from otree.api import *


doc = """
Dropout handler (general-use app)
"""


class C(BaseConstants):
    NAME_IN_URL = "dropout_handler"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    dropout = models.BooleanField(initial=False)


# PAGES
class Results(Page):
    @staticmethod
    def is_displayed(player):
        if "dropout" in player.participant.vars and player.participant.vars["dropout"]:
            player.dropout = True

            # by the way: you can run extra stuff in here as well
            # (here)

            if "dropout_guilty" in player.participant.vars:
                if player.participant.vars["dropout_guilty"]:
                    pass  # do something here if person is guilty
                else:
                    pass  # do something if partner dropped out
            else:
                pass  # individual-level game, subject dropped out

        return player.dropout

    @staticmethod
    def vars_for_template(player):
        if "dropout" in player.participant.vars and player.participant.vars["dropout"]:
            if "dropout_guilty" in player.participant.vars:
                guilty = player.participant.vars["dropout_guilty"]
            else:
                guilty = True
        else:
            guilty = False

        return dict(guilty=guilty)


page_sequence = [Results]
