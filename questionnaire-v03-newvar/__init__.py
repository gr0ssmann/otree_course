from otree.api import *


doc = """
A first questionnaire with feedback
"""


class C(BaseConstants):
    NAME_IN_URL = "questionnaire"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1
    GREETING = "Hello"


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    name = models.StringField(label="Please enter your first name.")
    age = models.IntegerField(
        label="Please enter your true age in years.", min=16, max=120
    )
    twice_the_age = models.IntegerField()


# PAGES
class MyPage(Page):
    form_model = "player"
    form_fields = ["name", "age"]

    @staticmethod
    def before_next_page(player, timeout_happened):
        player.twice_the_age = 2 * player.age


class Results(Page):
    pass


page_sequence = [MyPage, Results]
