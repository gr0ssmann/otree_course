from otree.api import *


doc = """
Dropout handler (general-use app)
"""


class C(BaseConstants):
    NAME_IN_URL = "dropout_handler"
    PLAYERS_PER_GROUP = None
    NUM_ROUNDS = 1


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    dropout = models.BooleanField(initial=False)


# PAGES
class Results(Page):
    @staticmethod
    def is_displayed(player):
        if "dropout" in player.participant.vars and player.participant.vars["dropout"]:
            player.dropout = True

            # by the way: you can run extra stuff in here as well
            # (here)

        return player.dropout


page_sequence = [Results]
