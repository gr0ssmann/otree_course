# oTree course

Example apps developed in [my](https://max.pm/) course on oTree held at the University of Cologne in 2024.

The class will be held on September 23rd, 24th, 30th and October 1st and 2nd, from 2pm to 6pm.

**The exam will be held on October 12, from 10am to 11am.** The exam's location is *101 Seminarraum S244* (*WiSo-Gebäude*).

The class is open to master's and doctoral students. External students can participate under some circumstances. [Please contact me.](https://max.pm/contact/)

## Structure

Most folders are named like this: `appname-vXX-comment`. This means: The name of the app is `appname`, the version is `XX` and the purpose of this version compared to others of `appname` is `comment`. A higher version number of an app does not necessarily mean that it is superior to previous versions, it merely indicates the sequence of development during my course. (The versions of an app can, to some extent, be viewed as levels of difficulty.)

The apps were developed in the following order:

1. `questionnaire`
1. `donation`
1. `pgg`
1. `decider`
1. `dropout_handler` (use with other apps)
1. `diary`
1. `calculate`
1. `conjoint`

In any case, it is instructive to use programs like `diff` to inspect the difference between versions.

## Preliminary schedule

### Monday (September 23)

1. Installing Python and oTree
1. Python basics (see below), getting started with `pip` and this repository
1. Using [black](https://github.com/psf/black)
1. Using oTree's command line interface
1. Fields and pages, basic validation
1. Developing and running a simple app
1. **PROJECT**: Survey
1. `vars_for_template`
1. Treatments
1. Static files
1. Currency amounts
1. **PROJECT**: Donation experiment
1. Currency
1. [translate](https://gitlab.com/gr0ssmann/translate)

### Tuesday (September 24)

1. Session settings
1. Models
1. `choices`
1. Basic grouping
1. Roles
1. Basic timeouts
1. **PROJECT**: Donation game with Choice Architect
1. Rounds
1. Templating with `if` and `for`
1. WaitPages
1. **PROJECT**: Public goods game with feedback

### Monday (September 30)

1. **PROJECT**: Decider game
1. Widgets
1. Subsessions, players and participants
1. `participant.vars`, shortcuts
1. Advanced timeouts
1. `devserver` vs `prodserver`
1. App sequences
1. Dropout handling
1. `app_after_this_page`
1. **PROJECT**: Beauty contest with matching on course of study

### Tuesday (October 1)

1. Advanced grouping/matching, rematching/regrouping
1. `js_vars`
1. Live Pages
1. **PROJECT**: Surveillance game
1. **PROJECT**: Sentinel fields
1. `ExtraModel`, Custom exports
1. Chats
1. **PROJECT**: Conjoint experiments with live pages

### Wednesday (October 2)

1. Rooms
1. WaitPages (advanced functions)
1. How to run a whole experiment at [CLER](https://cler.uni-koeln.de/en/)
1. Using other people's apps and code, e.g. [drypage](https://gitlab.com/gr0ssmann/drypage)
1. Documentation of apps and comments
1. How to deal with problems, how to get help, debugging

## Installing oTree

### Windows

1. If you have previously installed Python on your computer, go straight to step 6 and follow from there; if this does not work, go to step 2.
1. Download [this](https://www.python.org/ftp/python/3.11.0/python-3.11.0-amd64.exe) file and execute it.
1. Importantly, **check the following boxes**:
    - Use admin privileges when installing py.exe
    - Add python.exe to PATH
1. Click *Install Now*
1. Reboot your machine
1. Open the PowerShell (sometimes Win+Shift+1 works) and execute the following command: `python -m pip install otree` If this does not work, try `py -m pip install otree` or `pip install otree`
1. After completion, execute `otree --version` and verify that it says `5.10.1` or some other reasonably recent version

*Note*: On Windows, we will not use a virtual environment as compared to macOS or GNU/Linux. This is because on Windows, there is less of a chance to mess up existing software with `pip`.

### macOS

1. If you have previously installed Python on your computer, go straight to step 5 and follow from there; if this does not work, go to step 2.
1. Download [this](https://www.python.org/ftp/python/3.11.0/python-3.11.0-macos11.pkg) file and execute it.
1. Follow the instructions and install the software.
1. Reboot your machine
1. Open a terminal and execute the following commands:
```bash
python3 -m venv otree_env
source otree_env/bin/activate
pip3 install -U otree
```
1. After completion, execute `otree --version` and verify that it says `5.10.3` or some other reasonably recent version
1. When wishing to *run* oTree, run `source otree_env/bin/activate` beforehand

### GNU/Linux

1. Install Python 3.x using your distribution's package manager. Then, to *install*, run the following in the terminal:
```bash
python3 -m venv otree_env
source otree_env/bin/activate
pip3 install -U otree
```
*Note*: `python3` might need replacement with `python` and `pip3` with `pip` -- but please do so only if the above does not work **AND** you are certain that you have Python 3.x installed (`python --version`).

2. When wishing to *run* oTree, run `source otree_env/bin/activate` beforehand

## Useful tutorials for beginners

- *Required*: Chapters 2, 3, 4, 5 of the [official Python tutorial](https://docs.python.org/3/tutorial/)
- *Required*: [HTML](https://www.youtube.com/watch?v=bWPMSSsVdPk)
- Recommended: [JavaScript](https://www.youtube.com/watch?v=xwKbtUP87Dk)
- Recommended: [CSS](https://www.youtube.com/watch?v=yfoY53QXEnI)

#### Further resources for learning Python

- Our [python\_course](https://gitlab.com/gr0ssmann/python_course)
- [Python](https://www.youtube.com/watch?v=BVfCWuca9nw)
- [Python](https://www.youtube.com/watch?v=ZDa-Z5JzLYM)

## Miscellaneous

- [fish](https://fishshell.com)
- [oTree documentation](https://otree.readthedocs.io/en/latest/index.html)
- [What is the meaning of @staticmethod?](https://www.youtube.com/watch?v=rq8cL2XMM5M)
- [Computerphile on floating point numbers](https://www.youtube.com/watch?v=PZRI1IfStY0)
- [oTree example apps](https://s3.amazonaws.com/otreehub/browsable_source/1e38462b-46c7-4ca1-bca5-536462f90131/index.html)
- [oTree real-effort apps](https://www.otreehub.com/projects/otree-realeffort/)

### How to generate room labels

The following proposition follows trivially:

```python
import random
import string

length = 8
n = 64

for _ in range(n):
    print(
        "".join(
            random.choices(
                string.ascii_lowercase + string.ascii_uppercase + string.digits,
                k=length,
            )
        )
    )
```

## License

Unless otherwise noted, everything in this repository is licensed under [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/), or (at your option) any later version.

The picture in `_static/co2` is in the public domain (U.S. Government work).

The word list in `_static/eff_large_wordlist.txt` is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/us/), © Electronic Frontier Foundation ([source](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt), no changes were made).
