from otree.api import *


doc = """
A decider game with voice
"""


class C(BaseConstants):
    NAME_IN_URL = "decider"
    PLAYERS_PER_GROUP = 3
    NUM_ROUNDS = 1
    ENDOWMENT = cu(10)


class Subsession(BaseSubsession):
    pass


def creating_session(subsession):
    for group in subsession.get_groups():
        decider = group.get_player_by_id(1)

        decider.is_decider = True


class Group(BaseGroup):
    donation = models.CurrencyField(
        label="Final donation per group member", max=C.ENDOWMENT
    )


class Player(BasePlayer):
    proposal = models.CurrencyField(label="Your proposal", max=C.ENDOWMENT)
    voice = models.LongStringField(label="Your message to the decider")
    is_decider = models.BooleanField(initial=False)


# PAGES
class ProposerPage(Page):
    form_model = "player"
    form_fields = ["proposal", "voice"]

    @staticmethod
    def is_displayed(player):
        return not player.is_decider


class WaitPage1(WaitPage):
    pass


class DeciderPage(Page):
    form_model = "group"
    form_fields = ["donation"]

    @staticmethod
    def is_displayed(player):
        return player.is_decider

    @staticmethod
    def vars_for_template(player):
        data = []

        for p in player.group.get_players():
            if p.id_in_group != player.id_in_group:  # decider didn't propose
                data.append((p.id_in_group, p.proposal, p.voice))

        return dict(data=data)


class WaitPage2(WaitPage):
    @staticmethod
    def after_all_players_arrive(group):
        for p in group.get_players():
            p.payoff = C.ENDOWMENT - group.donation


class Results(Page):
    pass


page_sequence = [ProposerPage, WaitPage1, DeciderPage, WaitPage2, Results]
